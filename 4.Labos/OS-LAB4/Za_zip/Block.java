package hr.fer.os.lab4;

public abstract class Block implements Comparable<Block>{
	
	private int addr, length, id;
	
	public Block(int addr, int length, int id) {
		if(addr < 0 || length == 0)
			throw new IllegalArgumentException();
		
		this.addr = addr;
		this.length = length;
		this.id = id;
	}
	
	/**
	 * Used for the TreeSet sorting by LENGTH!!!
	 */
	@Override
	public int compareTo(Block o) {
		if(o == null)
			throw new NullPointerException();
		
		return Integer.compare(this.length, o.length);
	}
	
	/**
	 * Used for the Set OBJECT DIFFERENTIATION using ID!!!
	 */
	@Override
	public abstract boolean equals(Object obj);

	public int getAddr() {
		return addr;
	}

	public void setAddr(int addr) {
		this.addr = addr;
	}

	public int getLength() {
		return length;
	}

	public void setLength(int length) {
		this.length = length;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}
	
	
}
