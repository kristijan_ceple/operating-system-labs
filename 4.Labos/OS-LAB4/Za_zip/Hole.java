package hr.fer.os.lab4;

public class Hole extends Block {
	
	public Hole(int addr, int length, int id) {
		super(addr, length, id);
	}
	
	public Hole(Process process) {
		super(process.getAddr()
				, process.getLength()
				, process.getId()
				);
	}
	
	@Override
	public boolean equals(Object obj) {
		if(obj == null || !(obj instanceof Hole))
			throw new IllegalArgumentException();
	
		Hole other = (Hole) obj;
		return (this.getId() == other.getId());
	}
	
}
