package hr.fer.os.lab4;

import java.util.Scanner;

public class Simulacija {
	
	public static final int MEMORY_SIZE = 50;
	
	public static void main(String[] args) {
		Memory memory = new Memory(MEMORY_SIZE);
		
		try(Scanner sc = new Scanner(System.in)){
			while(true) {
				String input = sc.nextLine();
				
				if(input.equals("GC")){
					memory.garbageCollection();
					memory.print();
					continue;
				}
				
				String action; Integer number;
				
				try{
					String[]parts = input.split("\\s");
					action = parts[0];
					number = Integer.valueOf(parts[1]);
				} catch(IndexOutOfBoundsException ex) {
					System.out.println("Wrong input! Try again!");
					continue;
				}
				
				switch(action) {
				case "alloc":
					// Note: number is in this case the size of process to be added
				    memory.addProcess(number);
				    memory.print();
					break;
				case "dealloc":
					// Note: number is in this case the id of process to be removed
					try{
						memory.removeProcess(number);
					} catch(NullPointerException ex) {
						System.out.printf("Process of index %d doesn't exist!%n", number);
					}
					memory.print();
					break;
				default:
					System.out.println("Wrong input! Try again!");
					continue;
				}
			}
		}
	}

}
