package useless.classes;

import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.util.Scanner;

public class DynamicAllocation {
	
	private static final int DEFAULT_MEMORY_SIZE = 50;
	
	public static void main(String[] args) {	
		
		char[] memory =  new char[DEFAULT_MEMORY_SIZE];
		memory_init(memory);
		printMemory(memory);
		
		try(Scanner sc = new Scanner(System.in)){
			while(true) {
				String input = sc.next();
				switch(input) {
				case "Z":
					generator(memory);
					break;
				case "F":
					garbageCollection(memory);
					break;
				default:
					continue;
				}
			}
		}
	}
	
	private static void generator(char[] memory) {
		System.out.println("Generator!");
	
		printMemory(memory);
	}
	
	private static void garbageCollection(char[] memory) {
		System.out.println("garbageCollection!");
		
		printMemory(memory);
	}
	
	private static void memory_init(char[] memory) {
		for(int i = 0; i < memory.length; i++) {
			memory[i] = '-';
		}
	}
	
	private static void printMemory(char[] memory) {
		System.out.print(" ");
		for(int i = 1; i <= DEFAULT_MEMORY_SIZE; i++) {
			System.out.printf("%d", i%10);
		}
		System.out.println();
		
		System.out.print("[");
		for(char character : memory) {
			System.out.printf("%c", character);
		}
		System.out.print("]");
	}
	
	public static class Word{
		
		private int programNo;
		private boolean allocated;
		
		public Word(int programNo, boolean allocated) {
			this.programNo = programNo;
			this.allocated = allocated;
		}
		
		@Override
		public String toString() {
			String string;
			
			if(allocated)
				string = String.format(" %d ", programNo);
			else
				string = " - ";
			
			return string;
		}
	}
		
	public static class Hole{
		
		Word[] words;
		int address;
		
		public Hole(int length, int address) {
			
		}
		
	}
}
