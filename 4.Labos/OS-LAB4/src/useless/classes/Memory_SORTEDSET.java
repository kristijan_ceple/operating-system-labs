package useless.classes;

import java.util.SortedSet;
import java.util.TreeSet;

import hr.fer.os.lab4.Block;
import hr.fer.os.lab4.Hole;
import hr.fer.os.lab4.Process;

public class Memory_SORTEDSET {
	
	public static final boolean PROCESS = true;
	public static final boolean HOLE = false;
	
	private Word[] words;
	private SortedSet<Process> processes = new TreeSet<>();
	private SortedSet<Hole> holes = new TreeSet<>();
	private int lastId = 0;

	//Ckay
	public Memory_SORTEDSET(int size) {
		words = new Word[size];
		
		Hole whole = new Hole(0, words.length, lastId);
		holes.add(whole);
		
		for(int i = 0; i < words.length; i++)
			words[i] = new Word(lastId, HOLE);
		
		printMemory();
	}

	private void writeThrough(Block block) {
		int addr = block.getAddr();
		int length = block.getLength();
		int endAddr = addr+length-1;
		if(endAddr > words.length)
			throw new IndexOutOfBoundsException();
		
		int id = block.getId();
		boolean process = block instanceof Process;
		
		for(int i = addr; i <= endAddr; i++) {
			words[i].setId(id);
			words[i].setProcess(process);
		}
	}
	
	public void printDataStructs() {
		System.out.println();
		
		System.out.println("Holes:");
		System.out.println("[  Id\tAddr\tlen]");
		for(Hole hole : holes)
			System.out.printf("%4d\t%4d\t%4d%n", hole.getId()
					, hole.getAddr(), hole.getLength());
		
		System.out.println("Processes:");
		System.out.println("[  Id\tAddr\tlen]");
		for(Process process : processes)
			System.out.printf("%4d\t%4d\t%4d%n", process.getId()
					, process.getAddr(), process.getLength());
		
		System.out.println();
	}
	
	public void printMemory() {
		System.out.println();
		
		System.out.print(" ");
		for(int i = 1; i <= words.length; i++) {
			System.out.printf("%d", i%10);
		}
		System.out.println();
		
		System.out.print("[");
		for(Word word : words)
			System.out.print(word.toString());
		System.out.println("]");
		
		System.out.println();
	}
	
	public void addProcess(int length) {
		if(length <= 0)
			throw new IllegalArgumentException();
		
		Hole minHole = null;
		int delta = 0;
		for(Hole hole : holes) {
			int holeLength = hole.getLength();
			if(length <= holeLength) {
				minHole = hole;
				delta = holeLength-length;
				break;
			}
		}
		
		if(minHole == null) {
			System.out.println("Couldn't find a hole small enough!");
			return;
		}
		
		int addr = minHole.getAddr();
		Process toAdd = new Process(addr, length, lastId+1);
		lastId++;
		processes.add(toAdd);
		writeThrough(toAdd);
		
		if(delta != 0) {
			minHole.setAddr(addr+length);
			minHole.setLength(delta);
			writeThrough(minHole);
		} else {
			holes.remove(minHole);
		}
//		writeThrough(toAdd);
//		writeThrough();
	}
	
	public Hole returnHole(int id) {
		Hole toReturn = null;
		for(Hole findHole : holes)
			if(findHole.getId() == id) toReturn = findHole;
		
		if(toReturn == null)
			throw new IllegalArgumentException();
		else
			return toReturn;
	}
	
	public Process returnProcess(int id) {
		Process toReturn = null;
		for(Process findProc : processes)
			if(findProc.getId() == id) toReturn = findProc;
		
		if(toReturn == null)
			throw new IllegalArgumentException();
		else
			return toReturn;
	}
	
	public void removeProcess(int id) throws IllegalArgumentException{
		/* First we have to get the Process, and then find
		 * delete it, and make a hole in its place. Afterwards
		 * follows merging first the left, and then the right hole
		 * into our middle hole
		 */
		Process toRemove = returnProcess(id);
		
		// On to removing the process, and substituting a hole in its stead
		Hole substitution = new Hole(toRemove);
		processes.remove(toRemove);
		holes.add(substitution);
		
		//Let's check for merging
		//First on, LEFT MERGE
		int leftAddr = substitution.getAddr()-1;
		if(leftAddr >= 0 && !words[leftAddr].isProcess()) {
			int holeId = words[leftAddr].getId();
			Hole leftHole = returnHole(holeId);
				
			int newAddr = leftHole.getAddr();
			int lengthDelta = leftHole.getLength();
			
			holes.remove(leftHole);
			int newLength = lengthDelta + substitution.getLength();
			substitution.setAddr(newAddr);
			substitution.setLength(newLength);
		}
		
		int rightAddr = substitution.getAddr() + substitution.getLength();
		if(rightAddr < words.length && !words[rightAddr].isProcess()) {
			int holeId = words[rightAddr].getId();
			Hole rightHole = returnHole(holeId);
			
			int lengthDelta = rightHole.getLength();
			
			holes.remove(rightHole);
			int newLength = lengthDelta + substitution.getLength();
			substitution.setLength(newLength);
		}
		
		writeThrough(substitution);
	}
	
	public void Resize(Process process, Hole hole) {
		
	}
	
	//	####################	INNER HELP FUNCTION		##############################
	public class Word{
		
		private int id;
		private boolean process;
		
		public Word(int id, boolean process) {
			this.id = id;
			this.process = process;
		}
		
		@Override
		public String toString() {
			return process ? Integer.toString(id) : "-";
		}

		public int getId() {
			return id;
		}

		public void setId(int id) {
			this.id = id;
		}

		public boolean isProcess() {
			return process;
		}

		public void setProcess(boolean process) {
			this.process = process;
		}
	}
}
