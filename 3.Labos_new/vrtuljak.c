#define _XOPEN_SOURCE 500
#include <stdio.h>
#include <sys/types.h>
#include <sys/ipc.h>
#include <sys/sem.h>
#include <stdlib.h>
#include <unistd.h>
#include <signal.h>
#include <sys/wait.h>
#define foreach(op) for(int i = 0; i < seatsNo; i++){(op);}

//######################        SEMAPHORE OBJECT     #################################
#define SLEEP_CONST 2
#define NO_OF_SEMAPHORES 4
#define MJESTO 0
#define SJEO 1
#define VOZNJA 2
#define SISAO 3

int SemId, seatsNo = 0, processNo = 0;   /* identifikacijski broj skupa semafora */

void SemGet(int n)
{  /* dobavi skup semafora sa ukupno n semafora */
    SemId = semget(IPC_PRIVATE, n, 0600);

    if (SemId == -1) {
        printf("Nema semafora!\n");
        exit(1);
    }
}

int SemSetVal(int SemNum, int SemVal)
{  /* postavi vrijednost semafora SemNum na SemVal */
    return semctl(SemId, SemNum, SETVAL, SemVal);
}

int SemOp(int SemNum, int SemOp)
{  /* obavi operaciju SemOp sa semaforom SemNum */
    struct sembuf SemBuf;
    SemBuf.sem_num = SemNum;
    SemBuf.sem_op  = SemOp;
    SemBuf.sem_flg = 0;
    return semop(SemId, & SemBuf, 1);
}

void SemRemove(void)
{  /* uništi skup semafora */
    (void) semctl(SemId, 0, IPC_RMID, 0);
}

void waitSem(int SemNum)
{
    if( SemOp(SemNum, -1) == -1){
        printf("Greska prilikom cekanja na semafor!\n");
        exit(-1);
    }
}

void setSem(int SemNum)
{
    if( SemOp(SemNum, +1) == -1){
        printf("Greska prilikom cekanja na semafor!\n");
        exit(-1);
    }
}

//######################        SEMAPHORE OBJECT     #################################

void voznja()
{
    printf("\nVrtuljak krece s voznjom\n");
    sleep(SLEEP_CONST);
    printf("Vrtuljak zavrsava s voznjom\n\n");
}

void posjetitelj(int i)
{
    while(1){
        waitSem(MJESTO);
        printf("Process %d sjeo!\n", i);
        setSem(SJEO);
        waitSem(VOZNJA);
        printf("Process %d sisao!\n", i);
        setSem(SISAO);
    }
}

void vrtuljak()
{
    printf("Ja sam vrtuljak!\n");
    while(1){
        foreach( waitSem( SJEO ) )
        printf("Svi su sjeli!\n");
        voznja();
        foreach( setSem( VOZNJA ) )
        foreach( waitSem( SISAO ) )
        printf("Svi su sisli!\n");
        foreach( setSem( MJESTO ) )
    }
}

void prekiniVrtuljak( int sig )
{
    for(int i = 0; i < processNo; i++){
        wait(NULL);
    }

    SemRemove();
    exit(1);
}

int main(int argc, char* argv[])
{
    if(argc != 3){
        printf("#Cmd_arg != 3\n");
        exit(-1);
    }

    processNo = atoi(argv[1]);
    seatsNo = atoi(argv[2]);

    printf("Program start!\n");
    printf("#Process: %d\n", processNo);
    printf("#VrtuljakSeats: %d\n\n", seatsNo);

    SemGet(NO_OF_SEMAPHORES);
    SemSetVal(MJESTO, seatsNo);

    //semaphore starting value set to N
    //now it's time to fork() processNo processes
    //and then throw them all at the vrtuljak

    for(int i = 1; i <= processNo; i++){
        switch( fork() ){
            case 0:
                posjetitelj(i);
            default:
                continue;
        }
    }

    sigset( SIGINT, prekiniVrtuljak );
    vrtuljak();
}