#define _XOPEN_SOURCE 500
#include <stdio.h>
#include <stdlib.h>
#include <stdlib.h>
#include <signal.h>
#include <pthread.h>
#include <signal.h>
#include <unistd.h>

#define DATA_PATH "/home/kikyy99/CLionProjects/Nfilozofa/data.dat"
#define foreach(op, bound) for(int i = 0; i < (bound); i++) (op);
#define SLEEP_CONST 2


//  ###################     GLOBALNE VAR    ##############################
int noStapica = 0,  noFilozofa = 0;
int* philoArms = NULL;
FILE* dataStream = NULL;
pthread_t* thr_id = NULL;

pthread_mutex_t mut;
pthread_cond_t stapici;
//  ###################     GLOBALNE VAR    ##############################


//  ###################     POTREBNI PROTOTIPOVI    ##############################
void misliti(int);
void jesti(int);
//  ###################     POTREBNI PROTOTIPOVI    ##############################


//  ###################     MONITOR    ##############################
void uzeti_stapice(int threadIndex)
{
    pthread_mutex_lock(&mut);

    while(noStapica < philoArms[threadIndex]){
        printf("Filozof #%d. nema "
               "dovoljno stapica >:|\n", threadIndex);
        pthread_cond_wait(&stapici, &mut);
    }

    printf("Filozof #%d. zauzeo stapice(:%d)"
           " i ide jesti!\n", threadIndex, philoArms[threadIndex]);

    foreach(noStapica--, philoArms[threadIndex]);
    printf("Stapica ostalo na stolu: %d\n\n", noStapica);
    sleep(SLEEP_CONST);



    pthread_mutex_unlock(&mut);
}

void spustiti_stapice(int threadIndex)
{
    pthread_mutex_lock(&mut);

    printf("Filozof #%d. vratio stapice(:%d)!\n",
            threadIndex, philoArms[threadIndex]);

    foreach(noStapica++, philoArms[threadIndex])
    printf("Stapica ostalo na stolu: %d\n\n", noStapica);
    sleep(SLEEP_CONST);
    pthread_cond_signal(&stapici);

    pthread_mutex_unlock(&mut);
}
//  ###################     MONITOR    ##############################


//  ###################     FJE FILOZOFA    ##############################
void* Filozof(void *arg)
{
    int threadIndex = *((int *) arg);
    while(1){
        uzeti_stapice(threadIndex);
        jesti(threadIndex);
        spustiti_stapice(threadIndex);

        misliti(threadIndex);
    }
}

void misliti(int threadIndex)
{
    printf("Filozof #%d. razmislja!\n", threadIndex);
//    sleep(SLEEP_CONST);
}

void jesti(int threadIndex)
{
    printf("Filozof #%d. jede!\n", threadIndex);
//    sleep(SLEEP_CONST);
}
//  ###################     FJE FILOZOFA    ##############################


//  ###################     EXIT FJE    ##############################
void VarAndMemClean()
{
    fclose(dataStream);
    free(thr_id);
    free(philoArms);
    //TODO CLEAN OTHER RESOURCES
    //TODO DESTROY MUTEXES
    pthread_mutex_destroy(&mut);
    pthread_cond_destroy(&stapici);

    printf("Memory cleaned! Exiting...\n");
}

void prekid( int sig )
{
    printf("\nExit cause: SIGINT! ");
//    for(int i = 0; i < noFilozofa; i++){
//        pthread_join(thr_id[i], NULL);
//    }
    VarAndMemClean();
    exit(0);
}


//  ###################     EXIT FJE    ##############################


//  ###################    !!!MAIN!!!   ##############################
int main()
{
    printf("Program start! i-ti filozof je "
           "predstavljen i-tom dretvom.\n");

    dataStream = fopen(DATA_PATH, "r");
    if(dataStream == NULL){
        printf("dataStream is a null pointer!\n");
        exit(-1);
    }

    fscanf(dataStream, "%d %d", &noStapica, &noFilozofa);
    printf("#Stapici: %d\n", noStapica);
    printf("#Nfilozofa: %d\n", noFilozofa);
    philoArms = (int *) malloc(noFilozofa * sizeof(int));

    int data;
    int read = 0;

    while( fscanf(dataStream, "%d\n", &data) == 1 ){
        philoArms[read++] = data;
    }
    if( read != noFilozofa) {
        printf("Nedovoljan broj podataka!");
        VarAndMemClean();
        exit(-1);
    }
    foreach(printf("philoArms[%d] = %d\n", i, philoArms[i]), noFilozofa);

    //Tek sada krece pravo pisanje programa
    //Sada imam sve podatke koji mi trebaju na raspolaganju


    int threadIndex[noFilozofa];
    thr_id = (pthread_t *) malloc(noFilozofa * sizeof(pthread_t));
    sigset( SIGINT , prekid );

    //Monitor mutex and condition initialisation
    if( pthread_mutex_init(&mut, NULL) ){
        printf("Error during mutex initialization!");
        exit(-1);
    }
    if( pthread_cond_init(&stapici, NULL) ){
        printf("Error during condition initialization!");
        exit(-1);
    }

    for(int i = 1; i < noFilozofa; i++){
        threadIndex[i] = i;
        printf("Creating thread #%d...\n", i);
        if(pthread_create((thr_id + i), NULL, &Filozof,
                (void *)(&threadIndex[i])) != 0){
            printf("Error while creating threads!\n");
            exit(1);
        }
    }

    printf("Thread #0(Parent) becoming a Philosopher #0...\n\n");
    int arg = 0;
    Filozof((void *)(&arg));
}