//
// Created by kikyy99 on 19.10.18..
//
#include <stdlib.h>
#include <stdio.h>
#include <signal.h>
#include <unistd.h>
#include <sys/time.h>

void periodicki_posao ( int sig )
{
    printf ( "Radim periodicki posao\n" );
}

int main (void)
{
    struct itimerval t;
    struct sigaction act;

    /* povezivanje obrade signala SIGALRM sa funkcijom "periodicki_posao" */
    act.sa_handler = periodicki_posao;
    //sigemptyset(&act.sa_mask);
    sigaddset(&act.sa_mask, SIGQUIT);
    act.sa_flags = 0;

    /* definiranje periodičkog slanja signala */
    /* prvi puta nakon: */
    t.it_value.tv_sec = 2;
    t.it_value.tv_usec = 0;
    /* nakon prvog puta, periodicki sa periodom: */
    t.it_interval.tv_sec = 2;
    t.it_interval.tv_usec = 0;

    /* pokretanje sata s pridruženim slanjem signala prema "t" */
    setitimer ( ITIMER_REAL, &t, NULL );

    if(sigaction( SIGALRM, &act, NULL)){
        fprintf(stderr, "Nisam postavio masku za signale\n");
        perror("");
        exit(1);
    }

    while (1)
        pause (); /* pauzira do primitka bilo kojeg signala */

    return 0;
}
