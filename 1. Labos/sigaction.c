//
// Created by kikyy99 on 19.10.18..
//

#include <stdio.h>
#include <signal.h>
#include <stdlib.h>
#include <unistd.h>

void prekidna(int sig)
{
    printf("U obradi prekida\n");
    int x = 5;
    while(x = sleep(x));
    printf("Obrada prekida gotova\n\n");
}

int main() {
    struct sigaction act;

    act.sa_handler = prekidna;
    sigemptyset(&act.sa_mask);
    sigaddset(&act.sa_mask, SIGQUIT);
    act.sa_flags = 0;

    if(sigaction(SIGINT, &act, NULL)){
        fprintf(stderr, "Nisam postavio masku za signale\n");
        perror("");
        exit(1);
    }

    while(1);

    return 0;
}