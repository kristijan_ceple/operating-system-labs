#define _XOPEN_SOURCE 500
#include <stdio.h>
#include <signal.h>
#include <sys/types.h>
#include <sys/wait.h>
#include <sys/ipc.h>
#include <sys/shm.h>
#include <stdlib.h>
#include <unistd.h>
#include <fcntl.h>
#include <limits.h>
#include <stdio.h>
#include <pthread.h>
#include <sys/types.h>
#include <sys/ipc.h>
#include <sys/shm.h>
#include <sys/sem.h>
#include <sys/msg.h>
#include <values.h>
#include <pthread.h>
#include <stdbool.h>
#include <stdatomic.h>

#define TAS(ZASTAVICA) __atomic_test_and_set (&ZASTAVICA, __ATOMIC_SEQ_CST)

atomic_int A, M, N;
pthread_t *thr_id = NULL;
atomic_bool threadCont, zastavica = 0;

//CRITICAL SEGMENT FUNCTION
void KOFoo(int threadIndex, int* counter)
{   
    (*counter)++;
    A++;
    // printf("CRITICAL SEGMENT: Thread #%d - A++ = %d\n", threadIndex, A);
}

void NKOFoo(int threadIndex)
{
    // printf("NONCRITICAL SEGMENT: Thread #%d!\n\n", threadIndex);
}

void *thread( void *arg )
{
    int counter = 0;
    int threadIndex = *( (int *) arg );
    while(!threadCont);

    do{
        while(TAS(zastavica) != 0);
        KOFoo(threadIndex, &counter);
        zastavica = 0;

        NKOFoo(threadIndex);
    } while(counter < M);
    
    // printf("Thread #%d done --> A = %d\n", threadIndex, A);
    // printf("Thread exiting...\n\n");
    pthread_exit(NULL);
}

//######################        EXIT FUNCTIONS      ############################
void VarAndMemClean()
{
    printf("Before termination A = %d\n", A);
    free(thr_id);
    printf("Memory cleaned! Exiting...\n");
}

void ExitProgram()
{
    printf("Normal program exit.\n");
    for(int i = 0; i < N; i++){
        pthread_join(thr_id[i], NULL);
    }

    VarAndMemClean();
    exit(0);
}

void SigInt( int sig )
{
    printf("\nExit cause: SIGINT! ");
    VarAndMemClean();
    exit(1);
}
//######################        EXIT FUNCTIONS      ############################
int main(int argc, char* argv[]){
    if(argc != 3){
        printf("Wrong number of arguments: %d!\n", argc);
        exit(1);
    }
    N = atoi(argv[1]);
    M = atoi(argv[2]);
    sigset(SIGINT, SigInt);
    A = 0;

    int threadIndex[N];
    thr_id = (pthread_t *) malloc(N * sizeof(pthread_t));
    //DON'T FORGET TO FREE LATER!!!

    threadCont = false;
    for(int i = 0; i < N; i++){
        threadIndex[i] = i;
        // printf("Creating thread #%d...\n", i);
        if( pthread_create((thr_id + i), NULL, thread, (void *)(&threadIndex[i]) ) != 0){
            printf("Error while creating threads!\n");
            exit(1);
        }
    }
    threadCont = true;

    ExitProgram();
}