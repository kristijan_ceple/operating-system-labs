#define _XOPEN_SOURCE 500
#include <stdio.h>
#include <signal.h>
#include <sys/types.h>
#include <sys/wait.h>
#include <sys/ipc.h>
#include <sys/shm.h>
#include <stdlib.h>
#include <unistd.h>
#include <fcntl.h>
#include <limits.h>
#include <stdio.h>
#include <pthread.h>
#include "ExitProgram.h"

#define exitFunction (ExitProgram2)

int Id, *A, i, myForkRet;
/* myForkRet glavnog procesa cemo staviti na -1,
    a od djece na 0! */

void IncrementA(int M)
{
    for(int i = 0; i < M; i++){
        *A = *A + 1;
        printf("++A = %d M = %d\n", *A, i);
    }    
}

int main(int argc, char* argv[]){
    if(argc != 3){
        printf("Wrong number of arguments: %d!\n", argc);
        exit(1);
    }
    int N = atoi(argv[1]);
    int M = atoi(argv[2]);
    sigset(SIGINT, exitFunction);

    Id = shmget(IPC_PRIVATE, sizeof(int), S_IRWXU);
    if(Id == -1)
        exit(1);

    A = (int *) shmat(Id, NULL, 0);
    *A = 0;

    //za i = 0 ce biti ID parent procesa
    for(i = 1; i <= N; i++){
        myForkRet = fork();
        if(myForkRet == 0){
            printf("I'm a NEW process #%d\n", i);
            break;
        }
    }

    if(myForkRet != 0)
        i = 0;      //Parent proces

    
    printf("I'm process #%d\t"
    "Before incrementing A = %d\n", i, *A);
    // printf("I'm a process!\t");
    if(myForkRet == 0){
        IncrementA(M);
        printf("I'm process #%d and I've finished incrementing A! "
                "*A = %d, exiting...\n", i, *A);
        exit(0);
    }
        

    for(int i = 0; i < N; i++){
        (void) wait(NULL);
    }
    printf("I'm the parent process and I've closed all the children.\n"
            "A = %d\n", *A);
    exitFunction(0);

    // if( == 0){
    //         //ovo rade djeca!!! Dakle ona ce inkrementirati var
    //         IncrementVar();
    //     }
}