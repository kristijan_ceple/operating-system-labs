#include <signal.h>

#define FLOOR 10142  
#define CEILING 10152

int main(void){
    for(int i = FLOOR; i <= CEILING; i++){
        kill(i, SIGKILL);
    }
}