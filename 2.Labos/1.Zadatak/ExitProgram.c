#include <stdio.h>
#include <sys/types.h>
#include <sys/ipc.h>
#include <sys/shm.h>
#include <sys/sem.h>
#include <sys/msg.h>
#include <values.h>
#include <stdlib.h>
#include "ExitProgram.h"

extern int Id, *A, myForkRet, i;

void ExitProgram( int sig )
{
    printf("Izlazim zbog: %d\n", sig);
    FreeSHM();
    printf ( "Memorija ociscena! Izlazim iz programa...\n");
    exit(sig);
}

void ExitProgram2( int sig )
{
    printf("Termination cause: %d\n", sig);
    if(myForkRet == 0){
        printf("Terminating child process #%d!\n", i);
        exit(1);
    }
    
    (void) shmdt((char *) A);
    (void) shmctl(Id, IPC_RMID, NULL);
    printf ("Memory cleaned! Exiting...\n");
    exit(1);
}
