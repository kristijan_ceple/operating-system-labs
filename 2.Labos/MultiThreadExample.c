#define _XOPEN_SOURCE 500
#include <stdio.h>
#include <signal.h>
#include <sys/types.h>
#include <sys/wait.h>
#include <sys/ipc.h>
#include <sys/shm.h>
#include <stdlib.h>
#include <unistd.h>
#include <fcntl.h>
#include <limits.h>
#include <stdio.h>
#include <pthread.h>

int CommVar;

void *Writer1(void *x)
{
    CommVar = *( (int*) x );
}   

void *Reader1(void *x)
{
    int i;
    do{
        i = CommVar;
        printf("CommVar before do-while loop = %d\n", i);
        sleep(1);
    } while(i == 0);
    printf("CommVar after do-while loop = %d\n", i);
}

int main(void)
{
    int i;
    pthread_t thr_id[2];

    i = 123;
    CommVar = 0;
    
    if( pthread_create(&thr_id[0], NULL, *Reader1, NULL) != 0){
            printf("Error while creating threads!\n");
            exit(1);
    }
    
    if( pthread_create(&thr_id[1], NULL, *Writer1, &i) != 0){
            printf("Error while creating threads!\n");
            exit(1);
    }

    pthread_join(thr_id[0], NULL);
    pthread_join(thr_id[1], NULL);
    return 0;
}